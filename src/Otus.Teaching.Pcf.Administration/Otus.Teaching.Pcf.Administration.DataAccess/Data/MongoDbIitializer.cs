﻿using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class MongoDbIitializer
        : IDbInitializer
    {
        private readonly MongoContext _mongoContext;

        public MongoDbIitializer(MongoContext mongoContext)
        {
            _mongoContext = mongoContext;
        }

        public void InitializeDb()
        {
           // _mongoContext.Database.DropCollection("Emploee");
            //_mongoContext.Database.DropCollection("Role");

            _mongoContext.Database.CreateCollection("Employee");
            var employeeColl = _mongoContext.Database.GetCollection<Employee>("Employee");
            employeeColl.InsertMany(FakeDataFactory.Employees);


        }
    }

}
