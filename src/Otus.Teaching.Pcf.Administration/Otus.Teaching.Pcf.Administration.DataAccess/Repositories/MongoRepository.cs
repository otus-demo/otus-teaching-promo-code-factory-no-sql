﻿using MongoDB.Bson;
using MongoDB.Driver;
using Otus.Teaching.Pcf.Administration.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.Administration.Core.Domain;
using Otus.Teaching.Pcf.Administration.Core.Domain.Administration;
using SharpCompress.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Repositories
{
    public class MongoRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        private readonly MongoContext _context;

        public MongoRepository(MongoContext context)
        {
            _context = context;
        }

        public async Task AddAsync(T entity)
        {
            await _context.Database.GetCollection<T>("Employee").InsertOneAsync(entity);
        }

        public async Task DeleteAsync(T entity)
        {
            var filterDefinition = new BsonDocument { { "Id", entity.Id.ToString() } };
            await _context.Database.GetCollection<T>("Employee").DeleteOneAsync(filterDefinition);
        }

        public async Task<IEnumerable<T>> GetAllAsync()
        {
            return await _context.Database.GetCollection<T>("Employee").FindAsync<T>(new BsonDocument()).Result.ToListAsync<T>();
        }

        public async Task<T> GetByIdAsync(Guid id)
        {
            //var filterDefinition = new BsonDocument { { "Id", id.ToString() } };
            var filter = Builders<T>.Filter.Eq(new StringFieldDefinition<T, Guid>("_id"), id);
            return await _context.Database.GetCollection<T>("Employee").FindAsync<T>(filter).Result.FirstOrDefaultAsync<T>();
        }

        public Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetRangeByIdsAsync(List<Guid> ids)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate)
        {
            throw new NotImplementedException();
        }

        public Task UpdateAsync(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
